package pl.edu.uwm.wmii.annaswitaj.laboratorium04;
import java.lang.*;
import java.util.Scanner;
public class Zad1c {

    static String middle(String str)
    {
        if(str.length()%2==0)
        {
            int a=(str.length())/2-1;
            int b=(str.length())/2;
            String napis= Character.toString(str.charAt(a))+Character.toString(str.charAt(b));
            return napis;
        }

        else {
            int a = str.length() / 2;
            String napis=Character.toString(str.charAt(a));
            return napis;
        }

    }
    public static void main(String[] args) {
        Zad1c c= new Zad1c();
        System.out.println("Wprowadz napis: ");
        Scanner odczyt=new Scanner(System.in);
        String napis=odczyt.nextLine();
        System.out.println(c.middle(napis));


    }
}

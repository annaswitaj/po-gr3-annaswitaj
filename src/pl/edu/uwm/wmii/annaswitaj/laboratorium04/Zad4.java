package pl.edu.uwm.wmii.annaswitaj.laboratorium04;
import java.math.BigInteger;
public class Zad4 {
    static BigInteger zlicz(int n)
    {
        BigInteger ilosc=new BigInteger("0");
        BigInteger ziarno = new BigInteger("1");
        BigInteger mnoznik = new BigInteger("2");


            for(int j=0; j<n*n; j++){
                ilosc = ilosc.add(ziarno);
                ziarno=ziarno.multiply(mnoznik);
            }

        return ilosc;
    }

    public static void main(String[] args)
    {

        Zad4 zad=new Zad4();
        System.out.println(zad.zlicz(2));
    }
}

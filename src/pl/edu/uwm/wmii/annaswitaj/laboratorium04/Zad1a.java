package pl.edu.uwm.wmii.annaswitaj.laboratorium04;

public class Zad1a {
    static int countChar(String str,char c)
    {
        int zlicz=0;
        for(int i=0;i<str.length();i++)
        {
            if(str.charAt(i)==c)
            {
                zlicz++;
            }
        }

        return zlicz;
    }

    public static void main(String[] args) {
        Zad1a a= new Zad1a();
        System.out.println(a.countChar("czadshcgcgc",'c'));


    }
}
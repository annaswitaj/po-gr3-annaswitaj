package pl.edu.uwm.wmii.annaswitaj.laboratorium04;
import java.util.Scanner;
public class Zad1d {

    static String repeat(String str,int n)
    {

        String napis="";
        for(int i=0;i<n;i++)
        {
            napis+=str;
        }
        return napis;
    }

    public static void main(String[] args) {
        Zad1d d= new Zad1d();
        System.out.println("Wprowadz napis, jaki ma byc powtorzony: ");
        Scanner odczyt=new Scanner(System.in);
        String napis=odczyt.nextLine();
        System.out.println(d.repeat(napis,5));


    }
}

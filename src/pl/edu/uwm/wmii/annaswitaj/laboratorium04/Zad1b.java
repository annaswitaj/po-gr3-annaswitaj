package pl.edu.uwm.wmii.annaswitaj.laboratorium04;

import java.lang.*;
public class Zad1b {
    static int countSubStr(String str, String subStr)
    {
        int ilosc=0;
        for(int i=0;i<subStr.length();i++)
        {
            for (int j = 0; j < str.length()-subStr.length()+1; j++)
            {

                if (subStr.charAt(i) == str.charAt(j))
                {
                    String litery=str.substring(j,(j+ subStr.length()));

                    if(subStr.equals(litery)) {
                        ilosc++;
                    }
                }
            }
        }
        return ilosc;
    }
    public static void main(String[] args) {
        Zad1b b= new Zad1b();
        System.out.println(b.countSubStr("Ala ma kota,a kot ma Ale","Ale"));


    }
}

package pl.edu.uwm.wmii.annaswitaj.laboratorium04;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.String;

public class Zad3 {
    void zlicz(String sciezka,String subStr)
    {
        FileReader fr = null;
        String linia = "";

        // OTWIERANIE PLIKU:
        try {
            fr = new FileReader(sciezka);
        } catch (FileNotFoundException e) {
            System.out.println("BŁĄD PRZY OTWIERANIU PLIKU!");
            System.exit(1);
        }

        BufferedReader bfr = new BufferedReader(fr);
        // ODCZYT KOLEJNYCH LINII Z PLIKU:
        try {
            while((linia = bfr.readLine()) != null){
                System.out.println(linia);
                int ilosc=0;
                for(int i=0;i<subStr.length();i++)
                {
                    for (int j = 0; j < linia.length()-subStr.length()+1; j++)
                    {

                        if (subStr.charAt(i) == linia.charAt(j))
                        {
                            String litery=linia.substring(j,(j+ subStr.length()));

                            if(subStr.equals(litery)) {
                                ilosc++;
                            }
                        }
                    }
                }
                System.out.println(ilosc);
            }
        } catch (IOException e) {
            System.out.println("BŁĄD ODCZYTU Z PLIKU!");
            System.exit(2);
        }

        // ZAMYKANIE PLIKU
        try {
            fr.close();
        } catch (IOException e) {
            System.out.println("BŁĄD PRZY ZAMYKANIU PLIKU!");
            System.exit(3);
        }
    }


    public static void main(String[] args)
    {

        Zad3 zad=new Zad3();
        zad.zlicz("tekst.txt","ma");
    }
}

package pl.edu.uwm.wmii.annaswitaj.laboratorium04;

import java.math.BigDecimal;

public class Zad5 {
    static BigDecimal zlicz(double k, double p, int n) {
        BigDecimal kwota = new BigDecimal(k);

        BigDecimal odsetki = new BigDecimal(p*k);

        for (int j = 0; j < n; j++) {

            kwota = kwota.add(odsetki);

        }


        return kwota;
    }

    public static void main(String[] args) {

        Zad5 zad = new Zad5();
        System.out.println(zad.zlicz(1500, 0.01, 5));
    }
}

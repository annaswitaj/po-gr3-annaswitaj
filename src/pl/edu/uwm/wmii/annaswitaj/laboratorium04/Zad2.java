package pl.edu.uwm.wmii.annaswitaj.laboratorium04;
import java.io.*;
import java.io.File;
import java.io.IOException;


public class Zad2 {

    void zlicz(String sciezka,char znak)
    {
        FileReader fr = null;
        String linia = "";

        // OTWIERANIE PLIKU:
        try {
            fr = new FileReader(sciezka);
        } catch (FileNotFoundException e) {
            System.out.println("BŁĄD PRZY OTWIERANIU PLIKU!");
            System.exit(1);
        }

        BufferedReader bfr = new BufferedReader(fr);
        // ODCZYT KOLEJNYCH LINII Z PLIKU:
        try {
            while((linia = bfr.readLine()) != null){
                System.out.println(linia);
                int zlicz=0;
                for(int i=0;i<linia.length();i++)
                {
                    if(linia.charAt(i)==znak)
                    {
                        zlicz++;
                    }
                }
                System.out.println(zlicz);
            }
        } catch (IOException e) {
            System.out.println("BŁĄD ODCZYTU Z PLIKU!");
            System.exit(2);
        }

        // ZAMYKANIE PLIKU
        try {
            fr.close();
        } catch (IOException e) {
            System.out.println("BŁĄD PRZY ZAMYKANIU PLIKU!");
            System.exit(3);
        }
    }




    public static void main(String[] args)
    {

        Zad2 zad=new Zad2();
        zad.zlicz("tekst.txt",'a');
    }
}

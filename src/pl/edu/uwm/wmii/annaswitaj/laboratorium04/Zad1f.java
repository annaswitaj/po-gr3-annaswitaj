package pl.edu.uwm.wmii.annaswitaj.laboratorium04;
import java.util.*;
public class Zad1f {

    static String change(String str)
    {
        String s="";
        for(int i=0;i<str.length();i++)
        {
            String d=Character.toString(str.charAt(i));
            if(str.charAt(i)>='A'&& str.charAt(i)<='Z')
            {
                d=d.toLowerCase();
                s = new StringBuilder(s).append(d).toString();
            }
            else if(str.charAt(i)>='a'&&str.charAt(i)<='z')
            {
                d=d.toUpperCase();
                s=new StringBuilder(s).append(d).toString();
            }
        }
        return s;
    }
    public static void main(String[] args) {

        Zad1f f= new Zad1f();

        System.out.println(f.change("Ala ma kota,a kot ma Ale"));
    }

}

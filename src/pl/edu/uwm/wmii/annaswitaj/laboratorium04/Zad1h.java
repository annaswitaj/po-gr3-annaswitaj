package pl.edu.uwm.wmii.annaswitaj.laboratorium04;

public class Zad1h {
    static String nice(String str, char separator,int ile)
    {
        StringBuffer sb=new StringBuffer();
        char [] tab;
        tab=str.toCharArray();
        int j=0;
        for(int i=str.length()-1;i>=0;i--)
        {
            sb.append(tab[i]);
            if(j%ile == ile-1 && i!=0) sb.append(separator);
            j++;
        }
        sb = sb.reverse();
        return sb.toString();

    }


    public static void main(String[] args) {
        Zad1h h=new Zad1h();
        System.out.println(h.nice("Ala ma kota, a kot ma Ale",':',1));
    }

}

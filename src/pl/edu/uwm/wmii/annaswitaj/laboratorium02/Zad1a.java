package pl.edu.uwm.wmii.annaswitaj.laboratorium02;
import java.util.Random;
import java.util.Scanner;

public class Zad1a {

    public static void main(String[] args) {
        System.out.println("Podaj ilosc wczytywanych liczb: ");
        Scanner odczyt0=new Scanner(System.in);
        int n=odczyt0.nextInt();

        Random generator = new Random();
        int tab[]=new int [n];

        if(n>=1 && n<=100)
        {
            for(int i=0;i<n;i++)
            {
                tab[i]=generator.nextInt(1998)-999;
            }
        }
        else
            System.out.println("Liczba nie miesci sie w przedziale");


        int parzyste=0;
        int nieparzyste=0;
        for(int i=0;i<n;i++)
        {
            if(tab[i]%2==0)
                parzyste++;
            else
                nieparzyste++;
        }
        System.out.println("Ilosc liczb nieparzystych: "+nieparzyste);
        System.out.println("Ilosc liczb parzystych: "+parzyste);

    }
}
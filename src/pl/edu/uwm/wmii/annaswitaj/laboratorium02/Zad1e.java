package pl.edu.uwm.wmii.annaswitaj.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad1e {
    public static void main(String[] args) {
        System.out.println("Podaj ilosc wczytywanych liczb: ");
        Scanner odczyt0 = new Scanner(System.in);
        int n = odczyt0.nextInt();

        Random generator = new Random();
        int tab[] = new int[n];

        if (n >= 1 && n <= 100) {
            for (int i = 0; i < n; i++) {
                tab[i] = generator.nextInt(1998) - 999;
            }
        } else
            System.out.println("Liczba nie miesci sie w przedziale");

        for (int i = 0; i < n; i++) {
            System.out.println(tab[i] );
        }


        int dlugosc=0;
        int dlugosc2=0;
        int wynik;
        for (int i = 0; i < n; i++) {

            if(tab[i]>0 )  dlugosc ++;
            else if(tab[i]<=0 && dlugosc>dlugosc2) {
               wynik=dlugosc;
               dlugosc=dlugosc2;
               dlugosc2=wynik;
               dlugosc=0;
            }
            else dlugosc=0;

        }

        System.out.println("Najdluzszy fragment tablicy liczb dodatnich wynosi: "+dlugosc2);
    }
}
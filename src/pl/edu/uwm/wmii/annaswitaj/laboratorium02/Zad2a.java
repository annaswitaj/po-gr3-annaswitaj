package pl.edu.uwm.wmii.annaswitaj.laboratorium02;
import java.util.Random;
import java.util.Scanner;

public class Zad2a {


    public static void main(String[] args) {

        System.out.println("Podaj ilosc wczytywanych liczb: ");
        Scanner odczyt0 = new Scanner(System.in);
        int n = odczyt0.nextInt();

        if(n>=1 && n<=100) {
            int[] tab = new int[n];
            generuj(tab, n, -999, 999);
            //wyswietl(tab);
            ileParzystych(tab);
            ileNieparzystych(tab);
        }
        else
            System.out.println("Ilosc liczb nie miesci sie w przedziale!! n>=1 i n<=100");

    }

    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc) {
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
        }
    }

    public static void wyswietl(int tab[]) {
        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i] + " ");
        }
        System.out.println();
    }

    public static int ileNieparzystych(int tab[]){
        int a=0;
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]%2!=0)
                a++;

        }
        System.out.println("Ilosc liczb nieparzystych: "+a);
        return a;
    }

    public static int ileParzystych(int tab[]){
        int a=0;
        for(int i=0;i<tab.length;i++)
        {
            if(tab[i]%2==0)
                a++;

        }
        System.out.println("Ilosc liczb parzystych: "+a);
        return a;
    }

}

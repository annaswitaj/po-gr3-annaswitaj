package pl.edu.uwm.wmii.annaswitaj.laboratorium02;

import java.util.Scanner;
import java.util.Random;

public class Zad1b {
    public static void main(String[] args) {

        System.out.println("Podaj ilosc wczytywanych liczb: ");
        Scanner odczyt0=new Scanner(System.in);
        int n=odczyt0.nextInt();

        Random generator = new Random();
        int tab[]=new int [n];

        if(n>=1 && n<=100)
        {
            for(int i=0;i<n;i++)
            {
                tab[i]=generator.nextInt(1998)-999;
            }
        }
        else
            System.out.println("Liczba nie miesci sie w przedziale");

        int ujemne=0;
        int dodatnie=0;
        int zerowe=0;

        for(int i=0;i<n;i++)
        {
            if(tab[i]<0)
                ujemne++;
            else if(tab[i]>0)
                dodatnie++;
            else
                zerowe++;
        }
        System.out.println("Ilosc liczb ujemnych: "+ujemne);
        System.out.println("Ilosc liczb dodatnich: "+dodatnie);
        System.out.println("Ilosc liczb zerowych: "+zerowe);


    }
}
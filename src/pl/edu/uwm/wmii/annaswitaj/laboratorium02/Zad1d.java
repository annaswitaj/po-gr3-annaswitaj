package pl.edu.uwm.wmii.annaswitaj.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad1d {
    public static void main(String[] args) {
        System.out.println("Podaj ilosc wczytywanych liczb: ");
        Scanner odczyt0 = new Scanner(System.in);
        int n = odczyt0.nextInt();

        Random generator = new Random();
        int tab[] = new int[n];

        if (n >= 1 && n <= 100) {
            for (int i = 0; i < n; i++) {
                tab[i] = generator.nextInt(1998) - 999;
            }
        } else
            System.out.println("Liczba nie miesci sie w przedziale");

        int ujemne=0;
        int dodatnie=0;
        for (int i = 0; i < n; i++) {
            if(tab[i]>0)
                dodatnie+=tab[i];
            else if(tab[i]<0)
                ujemne+=tab[i];

        }


        System.out.println("suma ujemnych lementów tablicy to: "+ujemne);
        System.out.println("suma dodatnich elementów tablicy to : "+dodatnie);


    }
}
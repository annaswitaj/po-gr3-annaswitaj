package pl.edu.uwm.wmii.annaswitaj.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad1c {
    public static void main(String[] args) {
        System.out.println("Podaj ilosc wczytywanych liczb: ");
        Scanner odczyt0 = new Scanner(System.in);
        int n = odczyt0.nextInt();

        Random generator = new Random();
        int tab[] = new int[n];

        if (n >= 1 && n <= 100) {
            for (int i = 0; i < n; i++) {
                tab[i] = generator.nextInt(1998) - 999;
            }
        } else
            System.out.println("Liczba nie miesci sie w przedziale");

        int wynik=tab[1];
        for (int i = 0; i < n; i++) {
            if(tab[i]>wynik)
                wynik=tab[i];

        }
        int licznik=0;
        for (int i = 0; i < n; i++) {
            if(tab[i]==wynik)
                licznik++;

        }


        System.out.println("Najwiekszy element w tablicy to: "+wynik);
        System.out.println("Wystepuje : "+licznik+" razy");


    }
}
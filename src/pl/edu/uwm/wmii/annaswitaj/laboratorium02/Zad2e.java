package pl.edu.uwm.wmii.annaswitaj.laboratorium02;
import java.util.Random;
import java.util.Scanner;

public class Zad2e {


    public static void main(String[] args) {

        System.out.println("Podaj ilosc wczytywanych liczb: ");
        Scanner odczyt0 = new Scanner(System.in);
        int n = odczyt0.nextInt();

    if(n>=1 && n<=100) {
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        wyswietl(tab);
        dlugoscMaksymalnegoCiaguDodatnich(tab);

    }
    else
        System.out.println("Ilosc liczb nie miesci sie w przedziale!! n>=1 i n<=100");

    }

    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc) {
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
        }
    }

    public static void wyswietl(int tab[]) {
        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i] + " ");
        }
        System.out.println();
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[]){
        int dlugosc=0;
        int dlugosc2=0;
        int wynik;
        for (int i = 0; i < tab.length; i++) {
            if(tab[i]>0 ) {
                if(tab[i]==tab[tab.length-1]) {
                    dlugosc++;
                    dlugosc2 = dlugosc;
                }
                else dlugosc++;
            }
            else if(tab[i]<=0 && dlugosc>dlugosc2) {
                wynik=dlugosc;
                dlugosc=dlugosc2;
                dlugosc2=wynik;
                dlugosc=0;
            }

            else dlugosc=0;

        }

        System.out.println("dlugosc Maksymalnego Ciagu Dodatnich: "+dlugosc2);
        return dlugosc2;
    }



}
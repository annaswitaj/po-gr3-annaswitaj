package pl.edu.uwm.wmii.annaswitaj.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad2g {

    public static void main(String[] args) {

        System.out.println("Podaj ilosc wczytywanych liczb: ");
        Scanner odczyt0 = new Scanner(System.in);
        int n = odczyt0.nextInt();

        if (n >= 1 && n <= 100) {
            int[] tab = new int[n];
            generuj(tab, n, -999, 999);
            wyswietl(tab);
            odwrocFragment(tab);
            wyswietl(tab);

        } else
            System.out.println("Ilosc liczb nie miesci sie w przedziale!! n>=1 i n<=100");

    }

    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc) {
        Random r = new Random();
        for (int j = 0; j < n; ++j) {
            tab[j] = r.nextInt(maxWartosc - minWartosc + 1) + minWartosc;
        }
    }

    public static void wyswietl(int tab[]) {
        for (int i = 0; i < tab.length; i++) {
            System.out.print(tab[i] + " ");
        }
        System.out.println();
    }

    public static void odwrocFragment(int tab[]) {
        System.out.println("Podaj index lewy 1<=lewy<n : ");
        Scanner lewy = new Scanner(System.in);
        int le = lewy.nextInt();

        System.out.println("Podaj index prawy 1<=prawy<n : ");
        Scanner prawy = new Scanner(System.in);
        int p = prawy.nextInt();

        if (1 <= le && le <= p) {
            if((p-le+1)%2==0) {
                for (int i = le - 1, j = p - 1; i <= p / 2 - 1 && j >= p / 2; i++, j--) {
                    int temp = tab[i];
                    tab[i] = tab[j];
                    tab[j] = temp;
                }
            }
            else
                for (int i = le - 1, j = p - 1; i <= p / 2 - 1 && j >= (p / 2)+1; i++, j--) {
                    int temp = tab[i];
                    tab[i] = tab[j];
                    tab[j] = temp;

                }
            }

        }


    }


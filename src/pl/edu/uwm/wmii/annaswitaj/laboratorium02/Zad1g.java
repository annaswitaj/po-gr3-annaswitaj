package pl.edu.uwm.wmii.annaswitaj.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad1g {
    public static void main(String[] args) {
        System.out.println("Podaj ilosc wczytywanych liczb: ");
        Scanner odczyt0 = new Scanner(System.in);
        int n = odczyt0.nextInt();

        Random generator = new Random();
        int tab[] = new int[n];

        if (n >= 1 && n <= 100) {
            for (int i = 0; i < n; i++) {
                tab[i] = generator.nextInt(1998) - 999;
            }
        } else
            System.out.println("Liczba nie miesci sie w przedziale");

        for (int i = 0; i < n; i++) {
            System.out.print(tab[i] + " ");
        }
        System.out.println();

        System.out.println("Podaj index lewy 1<=lewy<n : ");
        Scanner lewy = new Scanner(System.in);
        int le = lewy.nextInt();

        System.out.println("Podaj index prawy 1<=prawy<n : ");
        Scanner prawy = new Scanner(System.in);
        int p = prawy.nextInt();

        if (1 <= le && le <= p) {
            if ((p - le + 1) % 2 == 0) {
                for (int i = le - 1, j = p - 1; i <= p / 2 - 1 && j >= p / 2; i++, j--) {
                    int temp = tab[i];
                    tab[i] = tab[j];
                    tab[j] = temp;
                    }
                    for (int k = 0; k < n; k++) {
                        System.out.print(tab[k]+" ");
                }
            }
            else {
                for (int i = le - 1, j = p - 1; i <= p / 2 - 1 && j >= (p / 2) + 1; i++, j--) {
                    int temp = tab[i];
                    tab[i] = tab[j];
                    tab[j] = temp;
                    }
                    for (int k = 0; k < n; k++) {
                        System.out.print(tab[k]+" ");
                }

            }
        }
        else
                System.out.println("Indexy musza miescic sie w przedziale 1<=lewy<n i 1<=prawy<n) ");

    }
}


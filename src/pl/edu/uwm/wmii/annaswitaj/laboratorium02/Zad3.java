package pl.edu.uwm.wmii.annaswitaj.laboratorium02;
import java.util.Random;
import java.util.Scanner;

public class Zad3 {
    public static void main(String[] args) {
        System.out.println("Podaj pierwsza liczbe z przedzialu [1;10]: ");
        Scanner liczba1 = new Scanner(System.in);
        int m = liczba1.nextInt();

        System.out.println("Podaj liczbe liczbe z przedzialu [1;10]: ");
        Scanner liczba2 = new Scanner(System.in);
        int n = liczba2.nextInt();

        System.out.println("Podaj trzecia liczbe z przedzialu [1;10]: ");
        Scanner liczba3 = new Scanner(System.in);
        int k = liczba3.nextInt();

        int  tabA[][]=new int [m][n];
        int  tabB[][]=new int[n][k];
        int  tabC[][]=new int [m][n];

        generowaniemacierzA(tabA);
        wyswietl(tabA);
        generowaniemacierzB(tabB);
        wyswietl(tabB);
        if(m==n)
        {
            mnozenie(tabA,tabB,tabC);
            wyswietl(tabC);
        }
        else
            System.out.println("Nie mozna wykonac mnozenia. Liczba kolumn macierzy A nie jest rowna liczbie wierszy macierzy B: ");
    }
    public static void generowaniemacierzA(int tab[][])
    {
        Random r = new Random();
        for(int i=0; i< tab.length; i++)
            for(int j=0; j< tab[i].length; j++)
                tab[i][j] = r.nextInt(10);
    }
    public static void generowaniemacierzB(int tab[][])
    {
        Random r = new Random();
        for(int i=0; i< tab.length; i++)
            for(int j=0; j< tab[i].length; j++)
                tab[i][j] = r.nextInt(10);
    }
    public static void mnozenie(int tabA[][],int tabB[][],int tabC[][])
    {
        for(int i=0; i<tabA.length; i++)
        {
            for(int j=0; j<tabA[i].length; j++)
            {
                for(int k=0; k<tabA.length; k++)
                {
                    tabC[i][j] += tabA[i][k] * tabB[k][j];
                }
            }
        }

    }
    public static void wyswietl(int tab[][])
    {
        for(int i=0; i< tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                System.out.print(tab[i][j]+" ");
            }
            System.out.println();
        }
        System.out.println();
    }
}

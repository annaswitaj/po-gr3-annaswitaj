package pl.edu.uwm.wmii.annaswitaj.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zad1f {
    public static void main(String[] args) {
        System.out.println("Podaj ilosc wczytywanych liczb: ");
        Scanner odczyt0 = new Scanner(System.in);
        int n = odczyt0.nextInt();

        Random generator = new Random();
        int tab[] = new int[n];

        if (n >= 1 && n <= 100) {
            for (int i = 0; i < n; i++) {
                tab[i] = generator.nextInt(1998) - 999;
            }
        } else
            System.out.println("Liczba nie miesci sie w przedziale");

        for (int i = 0; i < n; i++) {
            if(tab[i]>0)
                tab[i]=1;
            if(tab[i]<0)
                tab[i]=-1;

        }
        for (int i = 0; i < n; i++) {
            System.out.println(tab[i]);

        }



    }
}
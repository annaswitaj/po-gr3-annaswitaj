package pl.edu.uwm.wmii.annaswitaj.laboratorium07;

import pl.imiajd.switaj.Nauczyciel;
import pl.imiajd.switaj.Student;

public class TestOsoba {
    public static void main(String[] args)
    {
        Nauczyciel nauczyciel1=new Nauczyciel("Nowak",1980,2000);
        Student student1=new Student("Zaran",1997,"Informatyka");

        System.out.println(nauczyciel1.getNazwisko());
        System.out.println(nauczyciel1.getRok_urodzenia());
        System.out.println(nauczyciel1.getPensja());
        System.out.println(student1.getNazwisko());
        System.out.println(student1.getRok_urodzenia());
        System.out.println(student1.getKierunek());
    }
}

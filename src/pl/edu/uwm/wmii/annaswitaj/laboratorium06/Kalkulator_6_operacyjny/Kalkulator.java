package pl.edu.uwm.wmii.annaswitaj.laboratorium06;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Kalkulator {
    ArrayList<Float> c=new ArrayList<Float>();

    public static void main(String args[])
    {

        Dodawanie dod=new Dodawanie();
        Odejmowanie ode=new Odejmowanie();
        Mnozenie mno=new Mnozenie();
        Dzielenie dzie=new Dzielenie();
        Pierwiastek pie=new Pierwiastek();
        Potega pot=new Potega();
        Kalkulator kalk=new Kalkulator();
        System.out.println("Co chcesz wykonac ?");

        System.out.println("Menu wyboru \n");
        System.out.println ("[1]Dodawanie \n");
        System.out.println ("[2]Odejmowanie \n");
        System.out.println ("[3]Dzielenie \n");
        System.out.println ("[4]Mnozenie \n");
        System.out.println ("[5]Potega \n");
        System.out.println ("[6]Pierwiastek \n");
        System.out.println("Wpisz odpowiedni znak: ");
        Scanner odczyt=new Scanner(System.in);
        int wybor=odczyt.nextInt();
        switch(wybor)
        {
            case 1:
                try {
                    dod.doda();
                }
                catch(InputMismatchException e){
                    System.out.println("To nie jest liczba! albo wpisales '.' zamiast ','");
                }
                break;

            case 2:
                try {
                    ode.odejm();
                }
                catch(InputMismatchException e){
                    System.out.println("To nie jest liczba! albo wpisales '.' zamiast ','");
                }
                break;

            case 3:
                try {
                    dzie.dziel();
                }
                catch(InputMismatchException e){
                    System.out.println("To nie jest liczba! albo wpisales '.' zamiast ','");
                }
                break;

             case 4:
                 try {
                     mno.mnoz();
                 }
                 catch(InputMismatchException e){
                     System.out.println("To nie jest liczba! albo wpisales '.' zamiast ','");
                 }
                 break;

            case 5:
                try {
                    pot.pote();
                }
                catch(InputMismatchException e){
                    System.out.println("To nie jest liczba! albo wpisales '.' zamiast ','");
                }
                break;

            case 6:
                try {
                    pie.pier();
                }
                catch(InputMismatchException e){
                    System.out.println("To nie jest liczba! albo wpisales '.' zamiast ','");
                }
                break;

            default:
                System.out.println("Nie ma takiego wyboru!");
                    break;


        }

    }
}

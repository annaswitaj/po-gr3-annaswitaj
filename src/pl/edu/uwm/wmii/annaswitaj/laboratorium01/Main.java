package pl.edu.uwm.wmii.annaswitaj.laboratorium01;
import java.lang.*;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        Samochod samochod1=new Samochod("Volvo",23);
        Samochod samochod2=new Samochod("Passat",20);
        Samochod samochod3=new Samochod("Mitsubishi",21);
        ArrayList lista1=new ArrayList();
        lista1.add("Marka: "+samochod1.marka);
        lista1.add("Rocznik: "+samochod1.rocznik(2019));
        lista1.add("Pojemosc silnika: "+samochod1.pojSilnika);
        lista1.add("Spalanie na trasie: "+samochod1.obliczSpalanieNaTrasie(50));
        lista1.add("Marka: "+samochod2.marka);
        lista1.add("Rocznik: "+samochod2.rocznik(2000));
        lista1.add("Pojemosc silnika: "+samochod2.pojSilnika);
        lista1.add("Spalanie na trasie: "+samochod2.obliczSpalanieNaTrasie(23));
        lista1.add("Marka: "+samochod3.marka);
        lista1.add("Rocznik: "+samochod3.rocznik(1997));
        lista1.add("Pojemosc silnika: "+samochod3.pojSilnika);
        lista1.add("Spalanie na trasie: "+samochod3.obliczSpalanieNaTrasie(100));
        System.out.println(lista1);


    }

    }


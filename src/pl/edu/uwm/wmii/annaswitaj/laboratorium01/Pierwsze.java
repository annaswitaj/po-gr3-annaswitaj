package pl.edu.uwm.wmii.annaswitaj.laboratorium01;
import java.util.Scanner;
import static java.lang.Math.*;

public class Pierwsze {
    public static int zad1a(int n){
        int[] tab= new int[n];
        int wynik=0;
        System.out.print("Podaj liczby: ");
        for(int i=0;i<n;i++)
        {
            Scanner odczyt0=new Scanner(System.in);
            tab[i]=odczyt0.nextInt();
          wynik+=tab[i];
        }
        return wynik;
    }

    public static int zad1e(int n){
        int[] tab= new int[n];
        int wynik=1;
        System.out.print("Podaj liczby: ");
        for(int i=0;i<n;i++)
        {
            Scanner odczyt0=new Scanner(System.in);
            tab[i]=odczyt0.nextInt();
            wynik=wynik*Math.abs(tab[i]);
        }
        return wynik;
    }
    public static int zad1h(int n){
        int[] tab= new int[n];
        int wynik=0;

        System.out.print("Podaj liczby: ");
        for(int i=0;i<n;i++)
        {
            Scanner odczyt0=new Scanner(System.in);
            tab[i]=odczyt0.nextInt();
            double potega=Math.pow(-1,(i+1));
            wynik=wynik+(int)potega*tab[i];
        }
        return wynik;
    }

    public static void main(String[] args) {

        //System.out.println(Pierwsze.zad1a(5));
        //System.out.println(Pierwsze.zad1e(5));
        System.out.println(Pierwsze.zad1h(5));

    }
}


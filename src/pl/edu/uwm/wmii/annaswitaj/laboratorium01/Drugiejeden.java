package pl.edu.uwm.wmii.annaswitaj.laboratorium01;
import java.lang.*;
import java.util.Scanner;

public class Drugiejeden {
    public static int zad21b(int n){
        int[] tab= new int[n];
        int licznik=0;
        System.out.print("Podaj liczby: ");
        for(int i=0;i<n;i++)
        {
            Scanner odczyt0=new Scanner(System.in);
            tab[i]=odczyt0.nextInt();
            if(tab[i]%3==0&&tab[i]%5!=0){
              licznik++;
            }
        }
        return licznik;
    }

    public static int zad21d(int n){
        int[] tab = new int[n];
        int licznik = 0;
        System.out.print("Podaj liczby: ");
        for (int i = 0; i < n; i++) {
            Scanner odczyt0 = new Scanner(System.in);
            tab[i] = odczyt0.nextInt();

        }

        for(int i=0;i<n;i++)
        {
            if ((i > 0) && (i < n-1) && (tab[i] <( tab[i - 1] + tab[i + 1])/2))
            {
                licznik++;
            }
        }
        return licznik;
    }

    public static int zad21f(int n){
        int[] tab= new int[n];
        int licznik=0;
        System.out.print("Podaj liczby: ");
        for(int i=0;i<n;i++)
        {
            Scanner odczyt0=new Scanner(System.in);
            tab[i]=odczyt0.nextInt();

            if((i+1)%2!=0&&tab[i]%2==0){
                licznik++;
            }
        }
        return licznik;
    }
    public static void main(String[] args) {
        //System.out.println(Drugiejeden.zad21b(5));
        //System.out.println(Drugiejeden.zad21d(6));
        System.out.println(Drugiejeden.zad21f(5));

    }

}

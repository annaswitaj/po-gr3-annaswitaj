package pl.edu.uwm.wmii.annaswitaj.laboratorium03.zad1;

public class Wojownik extends Bohater{


    public Wojownik(String imie,int zywotnosc,int sila,int PT){
        name=imie;
        vitality=zywotnosc;
        skill=sila;
        tacticpoint=PT;
    }

    public String toString(){
        return"Imie: "+name+" Zywotnosc: "+vitality+"% Sila: "+skill+" Punkty taktyki: "+tacticpoint;
    }

    void zmiana_pkt_zycia() {
        super.zmiana_pkt_zycia();
        if(vitality<20 && vitality>0)
        {
            final int a=120;
            vitality=a;
        }
    }


}

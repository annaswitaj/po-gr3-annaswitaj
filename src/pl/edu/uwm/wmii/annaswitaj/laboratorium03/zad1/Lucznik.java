package pl.edu.uwm.wmii.annaswitaj.laboratorium03.zad1;
import java.lang.*;

public class Lucznik extends Bohater{


    public Lucznik(String imie,int zywotnosc,int zrecznosc,int PT){
        name=imie;
        vitality=zywotnosc;
        skill=zrecznosc;
        tacticpoint=PT;
    }

    public String toString(){
        return "Imie: "+name+" Zywotnosc: "+vitality+"% Zrecznosc: "+skill+" Punkty taktyki: "+tacticpoint;
    }



}
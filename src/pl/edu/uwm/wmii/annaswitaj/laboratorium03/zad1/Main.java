package pl.edu.uwm.wmii.annaswitaj.laboratorium03.zad1;
import java.lang.String;

public class Main {

    public static void main(String[] args) {
        //czy B moze byc w tym samym pliku?
        //Czy B musi byc w tym samym pakiecie?
        Bohater lucznik1=new Lucznik("Goblin",100,15,3);
        Bohater lucznik2=new Lucznik("Legolas",100,15,5);
        Bohater wojownik1=new Wojownik("Ork",100,15,1);
        Bohater wojownik2=new Wojownik("Aragorn",100,16,6);

        wojownik2.zmiana_pkt_zycia();
        System.out.println(wojownik2.toString());
        wojownik2.moc_ataku();
        System.out.println(wojownik2.toString());
        wojownik2.zmiana_pkt_zycia();
        System.out.println(wojownik2.toString());
    }
}

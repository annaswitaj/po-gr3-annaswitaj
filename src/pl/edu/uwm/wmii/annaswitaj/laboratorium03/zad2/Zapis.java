package pl.edu.uwm.wmii.annaswitaj.laboratorium03.zad2;
import java.io.*;
import java.sql.*;
import java.time.LocalDateTime;

public class Zapis{
    void zapis(double liczba,String sciezka){
        File file=new File(sciezka);

        try {
            if(file.exists()) {
                LocalDateTime aktualTime = LocalDateTime.now();
                PrintWriter zapis = new PrintWriter(new BufferedWriter(new FileWriter(file,true)));
                zapis.println(liczba+" Data i czas "+aktualTime);
                zapis.close();

            }
            else
                System.out.println("Plik nie istnieje");

        }
        catch (FileNotFoundException ex)
        {
         System.err.println("Plik nie istnieje");
        }

        catch (IOException ex)
        {
            System.err.println("File exists, but there was IOException");
        }
}
}
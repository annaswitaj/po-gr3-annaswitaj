package pl.edu.uwm.wmii.annaswitaj.laboratorium03.zad2;
import java.lang.*;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Main {
    public static void main(String[] args) {

        Konto kontoprywatne1=new KontoPrywatne(2000.65,"klientprywatny1.txt");
        Konto kontoprywatne2=new KontoPrywatne(5000,"klientprywatny2.txt");

        Konto kontofirmowe1=new KontoFirmowe(30000,"klientfirma1.txt");
        Konto kontofirmowe2=new KontoFirmowe(50000,"klientfirma2.txt");



        kontoprywatne1.platnosckarta(-50.2);
        kontoprywatne1.wyplatawbankomacie(-20.6);
        kontoprywatne1.wyplatawkasie(-45.7);
        ((KontoPrywatne) kontoprywatne1).otrzymaniezapoomogi(+300.34);
        kontoprywatne1.przelew(-39.60);
        ((KontoPrywatne) kontoprywatne1).przelewwynagrodzenia(+4890);
        kontoprywatne1.wplatawkasie(+99.74);

        kontoprywatne2.platnosckarta(-75.32);
        kontoprywatne2.wyplatawbankomacie(-400);
        kontoprywatne2.wyplatawkasie(-546.6);
        ((KontoPrywatne) kontoprywatne2).otrzymaniezapoomogi(+600.4);
        kontoprywatne2.przelew(-234);
        ((KontoPrywatne) kontoprywatne2).przelewwynagrodzenia(+2140);
        kontoprywatne2.wplatawkasie(+60);

        kontofirmowe1.wplatawkasie(+300);
        kontofirmowe1.przelew(-6000);
        kontofirmowe1.wyplatawkasie(-23442.23);
        kontofirmowe1.platnosckarta(-34259.3);
        ((KontoFirmowe) kontofirmowe1).przelewUS(-4000);
        ((KontoFirmowe) kontofirmowe1).przelewZUS(-4932.4);
        kontofirmowe1.wyplatawbankomacie(-6000);

        kontofirmowe2.wplatawkasie(+4365.4);
        kontofirmowe2.przelew(-2345);
        kontofirmowe2.wyplatawkasie(-235.23);
        kontofirmowe2.platnosckarta(-3494.3);
        ((KontoFirmowe) kontofirmowe2).przelewUS(-4040);
        ((KontoFirmowe) kontofirmowe2).przelewZUS(-4954.4);
        kontofirmowe2.wyplatawbankomacie(-1059);


    }
}

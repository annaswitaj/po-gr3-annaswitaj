package pl.edu.uwm.wmii.annaswitaj.laboratorium05;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class Main {
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer>b)
    {
        for(int i=0;i<b.size();i++)
        {
            a.add(b.get(i));
        }

        return a;
    }
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer>b)
    {
        ArrayList<Integer> c=new ArrayList<Integer>();
        int rozmiar=a.size()+b.size();
        for(int i=0;i<rozmiar;i++)
        {
            if(i%2==0&& i<2*a.size()) {
                c.add(a.get(i/2));
            }
            else if(i%2!=0&& i<2*b.size())
                c.add(b.get(i/2));
        }
        return c;
    }

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer>b)
    {
        Collections.sort(a);
        Collections.sort(b);
        Collections.max(a);
        Collections.max(b);
        int rozmiar;
        int wynikb=0;
        int wynika=0;
        int wynik;

        for(int i=0;i<a.size() ;i++)
        {

            if(Collections.max(b)<a.get(i)) wynika++;

        }

        for(int j=0;  j<b.size();j++)
        {
            if(Collections.max(a)<b.get(j)) wynikb++;

        }

        if(wynika>=wynikb) wynik=wynika;
        else wynik=wynikb;

        if(Collections.max(a)>=Collections.max(b)) rozmiar=a.size();
        else rozmiar=b.size();

        ArrayList<Integer> c=new ArrayList<Integer>();

        for(int i=0,j=0;i<a.size() && j<b.size();)
        {

            if(i==a.size()-1 || j==b.size()-1) break;
            else
                {
                if (a.get(i) < b.get(j)) {
                    c.add(a.get(i));
                        i++;


                }
                if (a.get(i) >= b.get(j)) {
                    c.add(b.get(j));
                        j++;

                }
            }
        }
        for (int i=rozmiar-wynik;i<rozmiar;i++) {
            if (wynik == wynikb)
                c.add(b.get(i));
            if (wynik == wynika)
                c.add(a.get(i));
        }
        return c;
    }

    public static ArrayList<Integer> reversed(ArrayList<Integer> a)
    {
        ArrayList<Integer> b=new ArrayList<Integer>();
        for(int i=a.size()-1;i>=0;i--)
        {
            b.add(a.get(i));
        }
        return b;
    }

    public static void reverse(ArrayList<Integer> a)
        {
            ArrayList<Integer> b=new ArrayList<Integer>();
            for(int i=a.size()-1;i>=0;i--)
            {
                b.add(a.get(i));
            }
            a=b;
            System.out.println(a);

    }
    public static void main(String[] args) {
        Main a=new Main();
        ArrayList<Integer> lista1=new ArrayList<Integer>();
        ArrayList<Integer> lista2=new ArrayList<Integer>();
        lista1.add(1);
        lista1.add(4);
        lista1.add(9);
        lista1.add(16);
        lista1.add(0);
        lista1.add(2);

        lista2.add(9);
        lista2.add(7);
        lista2.add(20);
        lista2.add(4);
        lista2.add(9);
        lista2.add(11);
        lista2.add(25);
        lista2.add(30);
        //System.out.println(a.append(lista1, lista2));
        //System.out.println(a.merge(lista1, lista2));
        System.out.println(a.mergeSorted(lista1, lista2));
        //System.out.println(a.reversed(lista1));
        //a.reverse(lista1);
    }
}
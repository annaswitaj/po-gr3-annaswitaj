package pl.imiajd.switaj;
import java.awt.Rectangle;

public class BetterRectangle extends Rectangle{

    public BetterRectangle(int x, int y,int wysokosc, int szerokosc)
    {
        this.setLocation(x,y);
        this.setSize(szerokosc,wysokosc);
    }


    public double  getPerimeter()
    {
        return 2*super.getHeight()+2*super.getWidth();
    }

    public double getArea()
    {
        return super.getHeight()*super.getWidth();
    }
}

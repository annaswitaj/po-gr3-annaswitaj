package pl.imiajd.switaj;

import pl.imiajd.switaj.Osoba;

public class Nauczyciel extends Osoba {

    float pensja;
    public Nauczyciel(String nazwisko,int rokurodzenia,float pensja)
    {
        this.nazwisko=nazwisko;
        this.rok_urodzenia=rokurodzenia;
        this.pensja=pensja;
    }

    public String toString()
    {
        return "Nazwisko: "+nazwisko+" Rok urodzenia: "+ rok_urodzenia+" Pensja: "+pensja+"zl";
    }

    public float getPensja()
    {
        return(pensja);
    }

}

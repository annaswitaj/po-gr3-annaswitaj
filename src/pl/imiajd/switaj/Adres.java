package pl.imiajd.switaj;

public class Adres {
    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private int[] kod_pocztowy;
    private Adres(String ulica,int numer_domu, int numer_mieszkania,String miasto,int[] kod_pocztowy)
    {
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.numer_mieszkania=numer_mieszkania;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }

    private Adres(String ulica,int numer_domu,String miasto,int[] kod_pocztowy)
    {
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }
    private void pokaz()
    {
        System.out.println("Kod pocztowy: "+kod_pocztowy+"Miasto: "+miasto);
        System.out.println("Ulica: "+ulica+"Numer domu: "+numer_domu+"Numer mieszkania"+numer_mieszkania);
    }

    public boolean przed(int szukany,int wybrany)
    {
        boolean a=false;
        if(szukany>wybrany)
        {
            a= true;

        }
        if(szukany<wybrany)
        {
            a= false;
        }

        return a;
    }
}

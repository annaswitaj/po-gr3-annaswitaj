package pl.imiajd.switaj;

import pl.imiajd.switaj.Osoba;

public class Student extends Osoba {
    String kierunek;
    public Student(String nazwisko,int rokurodzenia,String kierunek)
    {
        this.nazwisko=nazwisko;
        this.rok_urodzenia=rokurodzenia;
        this.kierunek=kierunek;
    }
    public String toString()
    {
        return "Nazwisko: "+nazwisko+" Rok urodzenia: "+ rok_urodzenia+" Kierunek: "+kierunek+"zl";
    }

    public String getKierunek()
    {
        return(kierunek);
    }
}
